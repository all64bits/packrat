#!/usr/bin/env bash

# first arg is option
# second arg is path of the file to change
# third arg is replacement checksum (for "-u" option)
# '-u': update checksum (provide new checksum as arg)
# '-r': revert to checksum placeholder

if [ "$1" == '-u' ]; then
  sed -i -E "s/iso_checksum\": \"&&&&checksum&&&&\"/iso_checksum\": \"$3\"/g" "$2"; fi

if [ "$1" == '-r' ] && ! grep -iq 'iso_checksum": "&&&&checksum&&&&' "$2"; then
  sed -i -E 's/iso_checksum": "[^"]*/iso_checksum": "\&\&\&\&checksum\&\&\&\&/g' "$2"; fi
