# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure(2) do |config|

  config.vm.provider :virtualbox do |v, override|

	# After installation, Arch can run on a minimum of 512MB RAM
    v.customize ["modifyvm", :id, "--memory", 512]
  end

end

